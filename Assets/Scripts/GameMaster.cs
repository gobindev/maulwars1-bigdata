using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
	public GameObject enemy1;

	public GameObject canvas;
	UIController uIController;
	int groundScale = 5;

	int lives = 100;
	int gold = 1240;
	int income = 0;
	//int tower1cost = 10, tower2cost = 30, tower3cost = 80;
	
	/* 0 = currentpath
	*  1 = ground
	*  2 = wall
	*  3 = start
	*  4 = end
	*  5 = tower0
	*  6 = tower1
	*  7 = tower2
	*  8 = tower3
	*/
	
	int mapsize, mapdepht;
	//int[,] array1 = {{2,1,1,3,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,4,1,1,2}};
	//int[,] freemap;
	Dictionary<Vector2, int> freemap;
	Dictionary<Vector2, Tower1> towermap;
	public Dictionary<int, TowerClass> towercollection;
	public Dictionary<int, EnemyClass> enemycollection;
	int startx, starty, endx, endy;
	List<GameObject> objects = new List<GameObject>();
	//GameObject[,] grounds;
	Dictionary<Vector2, GameObject> grounds = new Dictionary<Vector2, GameObject>();
	GameObject startportal, endportal;
	
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
	
    void Update()
    {
		if (Input.GetMouseButtonDown(0)){

            Debug.Log("Pressed primary mouse button.");
			mouseClick();
		}

        //elapsed += Time.deltaTime;
    }
	int gamemode = 1;
	int pausetimer = 200, pausetime = 200;
	int spawntime = 300;
	int spawntimer = 0;
	int enemycount = 0;
	int enemiesmax = 0, enemies = 0;
	public List<Vector2> wavelist = new List<Vector2>();
	int wavecount = 1;
	public void setWave(List<Vector2> wave, int pauseMS){
		wavelist = new List<Vector2>(wave);
		pausetime = pauseMS;
		pausetimer = pauseMS;
		enemiesmax = wave.Count;
		uIController.setWave(wavecount);
	}
	public void setTowers(Dictionary<int, TowerClass> list){
		towercollection = list;
		int steps = 5;
		if(list.Count < 5){
			steps = list.Count;
		}
		for(int ii = 1; ii<=steps; ii++){
			uIController.changeTowerButtonText(ii, towercollection[ii], false);
		}

	}

	public void setEnemies(Dictionary<int, EnemyClass> list){
		enemycollection = list;
		int steps = 6;
		if(list.Count < 6){
			steps = list.Count;
		}
		for(int ii = 1; ii<=steps; ii++){
			//uIController.changeEnemyButtonText(ii, shortText(enemycollection[ii]), enemycollection[ii].color);
		}

	}

	string shortText(EnemyClass ec){
		string str = ec.name + " : " + (ec.goldForKill*5).ToString() + "g";
		return str;
	}
	string shortText(TowerClass tc){
		string str = tc.name + " : " + tc.coast.ToString() + "g";
		return str;
	}


	public int gameModeFlowController(){
		if(gamemode == 1){
			if(pausetimer<=0){
				pausetimer = pausetime;
				beginWave();
				return wavecount;
			}
			pausetimer--;
			if(pausetimer<=30){
				uIController.setScreenText((pausetimer/10+1).ToString());
			}
			uIController.setTime(pausetimer);
			
		}
		if(gamemode == 2){
			if(spawntimer <= 0){
				spawntime = (int)wavelist.ToArray()[enemies].y;
				spawntimer = spawntime;
				spawnEnemy((int)wavelist.ToArray()[enemies].x, false);
				enemies++;
			}
			uIController.setTime(spawntimer);
			spawntimer--;
			if(enemies>=enemiesmax){
				enemies = 0;
				gamemode = 3;
			}
		}
		return wavecount;
	}

	void beginWave(){

		roudStartAudio.Play();
		uIController.setScreenText("Go!");
		startportal.SetActive(true);
		endportal.SetActive(true);
		addGold(income);
		gamemode = 2;
		calcPath();
		if(buymode){
			if(freemap[new Vector2(currentMouseX, currentMouseY)] == 1){
				grounds[new Vector2(currentMouseX,currentMouseY)].GetComponent<MeshRenderer>().material.color = currentColor;
			}
			buymode = false;
			uIController.changeTowerButtonText(buyTowerNo, towercollection[buyTowerNo], false);
			buyTowerNo = 0;
		}
		
		foreach(Vector2 coord in props.bestPath){
			if(freemap[new Vector2(coord.x,coord.y)] == 1){
				freemap[new Vector2(coord.x,coord.y)] = 0;
				grounds[coord].GetComponent<MeshRenderer>().material.color = new Color(1,.85f,.7f);
			}
		}

				calcWallSpawn();
				freemap[wallSpawnPoint] = 0;
				grounds[wallSpawnPoint].GetComponent<MeshRenderer>().material.color = new Color(1,.85f,.7f);
		
	}
	void endWave(){

		uIController.setScreenText("Finish Wave!");
		startportal.SetActive(false);
		endportal.SetActive(false);
		gamemode = 1;
		wavecount++;
		setWave(Database.getNextWave(), 150);
		roudFinishAudio.Play();
			foreach(Vector2 coord in props.bestPath){
				if(freemap[new Vector2(coord.x,coord.y)] == 0){
					freemap[new Vector2(coord.x,coord.y)] = 1;
					grounds[coord].GetComponent<MeshRenderer>().material.color = currentColor;
				}
				
			}
		buildSpawnWall();
	}
	void buildSpawnWall(){
		grounds.Remove(wallSpawnPoint);

		GameObject clone = Instantiate(Database.worldObjects[Database.WorldObjectType.Wall], new Vector3(groundScale*wallSpawnPoint.x,0,groundScale*wallSpawnPoint.y), transform.rotation);
		clone.GetComponent<Ground>().init(wallSpawnPoint,this);
		freemap[wallSpawnPoint] = 2;
		grounds[wallSpawnPoint] = clone;
		objects.Add(clone);
	}

	
	public void spawnEnemy(int id, bool payment){
		if(enemycollection.Count >= id && gamemode != 1){
			if(payment){
				int price = enemycollection[id].goldForKill * 5;
				if(gold >= price){
					addGold(-price);
					addIncome(enemycollection[id].goldForKill);
					GameObject newenemy = Instantiate(enemycollection[id].instantiateNew(), new Vector3(groundScale*startx,.5f,groundScale*starty), transform.rotation);
					newenemy.GetComponent<Enemy1>().spawn(props.bestPath, this);
					props.enemyList.Add(newenemy);
				}
			}
			else if(!payment){
				GameObject newenemy = Instantiate(enemycollection[id].instantiateNew(), new Vector3(groundScale*startx,.5f,groundScale*starty), transform.rotation);
				newenemy.GetComponent<Enemy1>().spawn(props.bestPath, this);
				props.enemyList.Add(newenemy);
			}
			spawnEnemyAudio.Play();

		}
		
	}

	public void killEnemy(int g){
		addIncome(g/4);
		addGold(g);
		if(props.enemyList.Count == 0 && gamemode == 3){
			endWave();
		}
		killEnemyAudio.Play();
	}

	public void addIncome(int g){
		income += g;
		uIController.setIncome(income.ToString());
	}
	
	public void createWorld(int[,] array1, int size)
	{
		mapsize = size;
		mapdepht = array1.Length/mapsize;
		freemap = new Dictionary<Vector2, int>();
		towermap = new Dictionary<Vector2, Tower1>();
		Vector2 coord = new Vector2(0,0);
		foreach(int block in array1)
		{
			
				if(block == 1) //ground
				{
					GameObject clone = Instantiate(Database.worldObjects[Database.WorldObjectType.Ground], new Vector3(groundScale*coord.x,0,groundScale*coord.y), transform.rotation);
					clone.GetComponent<Ground>().init(coord,this);
					//int[] co = {groundScale*x,groundScale*y};
					//objs.Add(aa, co);
					freemap.Add(coord, 1);
					grounds.Add(coord, clone);
					objects.Add(clone);
				}
				else if(block == 2) //wall
				{
					GameObject clone = Instantiate(Database.worldObjects[Database.WorldObjectType.Wall], new Vector3(groundScale*coord.x,0,groundScale*coord.y), transform.rotation);
					clone.GetComponent<Ground>().init(coord,this);
					freemap.Add(coord, 2);
					grounds.Add(coord, clone);
					objects.Add(clone);
				}
				else if(block == 3)
				{
					GameObject clone = Instantiate(Database.worldObjects[Database.WorldObjectType.Start], new Vector3(groundScale*coord.x,0,groundScale*coord.y), transform.rotation);
					clone.GetComponent<Ground>().init(coord,this);
					startportal = Instantiate(Database.worldObjects[Database.WorldObjectType.Startportal], new Vector3(groundScale*coord.x,2.5f,groundScale*coord.y), transform.rotation);
					startportal.SetActive(false);
					freemap.Add(coord, 3);
					grounds.Add(coord, clone);
					objects.Add(clone);
					startx = (int)coord.x;
					starty = (int)coord.y;

				}
				else if(block == 4)
				{
					GameObject clone = Instantiate(Database.worldObjects[Database.WorldObjectType.End], new Vector3(groundScale*coord.x,0,groundScale*coord.y), transform.rotation);
					clone.GetComponent<Ground>().init(coord,this);
					endportal = Instantiate(Database.worldObjects[Database.WorldObjectType.Endportal], new Vector3(groundScale*coord.x,1.4f,groundScale*coord.y), transform.rotation);
					endportal.SetActive(false);
					freemap.Add(coord, 4);
					grounds.Add(coord, clone);
					objects.Add(clone);
					endx = (int)coord.x;
					endy = (int)coord.y;
				}
				coord = coord + new Vector2(1,0);
				if(coord.x == mapsize)
				{
					coord = new Vector2(0,coord.y+1);
				}
			
		}
		currentColor = grounds[new Vector2(1,1)].GetComponent<MeshRenderer>().material.color; //gefährlich, kann crashen wenn nicht vorhanden
		uIController = canvas.GetComponent<UIController>();
		uIController.setLives(lives);
		uIController.setGold(gold);
	}
	
	bool fixTooltip = false;
	Vector2 toolTipCoord = new Vector2();
	void mouseClick(){
		if(buymode && buyTowerNo != 0){
			buyTower();
		}
		if (!buymode){
			if(toolTipCoord == new Vector2(currentMouseX, currentMouseY)){
					lockClickedObject(toolTipCoord, false);
				}
			else if(towermap.ContainsKey(new Vector2(currentMouseX,currentMouseY))){
				lockClickedObject(new Vector2(currentMouseX,currentMouseY), true);
			}
			else{
				StartCoroutine(waitingForUnlock(0.05f));
			}
		}
	}

	IEnumerator waitingForUnlock(float s){
		yield return new WaitForSeconds(s);
		lockClickedObject(toolTipCoord, false);
	}

	bool buymode = false;
	int buyTowerNo = 0;

	void lockClickedObject(Vector2 coord, bool locking){

			setTooltip(coord);
			fixTooltip = locking;
		if(locking) {
			toolTipCoord = new Vector2(coord.x, coord.y);
			uIController.fixTooltip(towermap[coord], true);
		}
		else {
			toolTipCoord = new Vector2();
			uIController.fixTooltip(null, false);
		}
	}

	void buyTower(){
			buymode = false;
			if(freemap[new Vector2(currentMouseX,currentMouseY)] == 1){
				if(buyTowerNo>0 && buyTowerNo<=towercollection.Count){
					spawnTower(new Vector2(currentMouseX,currentMouseY), buyTowerNo);
					createTowerAudio.Play();
				}
				grounds[new Vector2(currentMouseX,currentMouseY)].GetComponent<MeshRenderer>().material.color = currentColor;
			}
			uIController.changeTowerButtonText(buyTowerNo, towercollection[buyTowerNo], false);
			buyTowerNo = 0;
	}
	public AudioSource createTowerAudio,destroyTowerAudio,upgradeTowerAudio,spawnEnemyAudio,killEnemyAudio,looseLiveAudio,shoot1Audio,hit1Audio,roudStartAudio,roudFinishAudio;
	public void spawnTower(Vector2 coord, int id){
					freemap[coord] = 10+id;
					GameObject newtower = Instantiate(towercollection[id].towerGameObject, new Vector3(groundScale*coord.x,1.5f,groundScale*coord.y), transform.rotation);
					towercollection[id].instantiateNew(newtower.GetComponent<Tower1>());
					newtower.GetComponent<Tower1>().build(this, (int)coord.x,(int)coord.y);
					addGold(-newtower.GetComponent<Tower1>().coast);
					towermap.Add(coord, newtower.GetComponent<Tower1>());
					StartCoroutine(spawnAnimation(coord));
	}
	IEnumerator spawnAnimation(Vector2 coord){
		GameObject anim = Instantiate(Database.worldObjects[Database.WorldObjectType.Upgrade], new Vector3(groundScale*coord.x,2.5f,groundScale*coord.y), transform.rotation);
		yield return new WaitForSeconds(3);
        Destroy(anim);
	}

	public void createTower(int id){
		if(buyTowerNo == id){
			uIController.changeTowerButtonText(buyTowerNo, towercollection[buyTowerNo], false);
			buyTowerNo = 0;
			buymode = false;
		}
		else if(gold >= towercollection[id].coast){
			buymode = true;
			buyTowerNo = id;
			currentTowerColor = Database.elementColors[towercollection[id].element];
			uIController.changeTowerButtonText(id, towercollection[buyTowerNo], true);
		}
		lockClickedObject(toolTipCoord, false);
	}

	public void destroyTower(Vector2 coord, int gold){
		towermap.Remove(coord);
		freemap[coord] = 1;
		addGold(gold);
		if(coord == toolTipCoord){
			lockClickedObject(toolTipCoord, false);
		}
		destroyTowerAudio.Play();
	}

	void calcPath(){
		props.pathes = new List<way>();
		props.coordinaten = new List<Vector2>();
		props.pathes.Add(new way(startx, starty, new List<Vector2>()));
		bool foundPath = false;
		props.freemap = new Dictionary<Vector2, int>(freemap);
		props.mapscaleX = mapsize;
		props.mapscaleY = mapdepht;
		props.goal = new Vector2(endx, endy);

		int rr = 0;
		while(!foundPath && rr<30){
			foundPath = iterateOverWays();
			rr++;
		}
	}

	Vector2 wallSpawnPoint;
	void calcWallSpawn(){
		int count = 0;
		foreach(KeyValuePair<Vector2,int> field in freemap){
			if(field.Value == 1){
			count++;
			}
		}
		count = Random.Range(1, count+1);
		int step = 0;
		foreach(KeyValuePair<Vector2,int> field in freemap){
			if(field.Value == 1){
			step++;
			if(step == count){
				wallSpawnPoint = field.Key;
			}
			}
		}
	}

	bool iterateOverWays(){
		List<way> pathesClone = new List<way>(props.pathes);
		foreach(way pat in pathesClone){
				if(pat.checkNextStep()){
					return true;
				}
			}
			return false;
	}

	public void looseLive(){
		lives--;
		uIController.setLives(lives);
		looseLiveAudio.Play();
	}

	public void addGold(int amount){
		gold = gold + amount;
		uIController.setGold(gold);
	}

	int currentMouseX = 0, currentMouseY = 0;
	Color currentColor;
	Color currentTowerColor;
	public void mouseOverCoordinates(int x, int y){
		if(x != currentMouseX || y != currentMouseY){
			if(buymode){
				if(freemap[new Vector2(currentMouseX, currentMouseY)] == 1){
					grounds[new Vector2(currentMouseX,currentMouseY)].GetComponent<MeshRenderer>().material.color = currentColor;
				}
				currentMouseX = x;
				currentMouseY = y;
				if(freemap[new Vector2(x,y)] == 1){
					//currentColor = grounds[new Vector2(currentMouseX,currentMouseY)].GetComponent<MeshRenderer>().material.color;
					grounds[new Vector2(currentMouseX,currentMouseY)].GetComponent<MeshRenderer>().material.color = currentTowerColor;
				}
			}
			currentMouseX = x;
			currentMouseY = y;
			if(!fixTooltip){
				setTooltip(new Vector2(x,y));
			}
			

			
			
		}
	}
	void setTooltip(Vector2 coord){
	
				if(freemap[coord] == 1){
					uIController.setTowerinfo("Empty Ground","");
				}
				else if(freemap[coord] == 0){
					uIController.setTowerinfo("Blocked Ground","");
				}
				else if(freemap[coord] == 2){
					uIController.setTowerinfo("Wall","");
				}
				else if(freemap[coord] == 3){
					uIController.setTowerinfo("Start Point","");
				}
				else if(freemap[coord] == 4){
					uIController.setTowerinfo("End Point","");
				}
				else if(freemap[coord] >= 10){
					string str = "";
					str += Tower1.towerposiitons[coord].towerHead.getInfo("effect") + "\n";
					str += Tower1.towerposiitons[coord].towerHead.getInfo("range") + "\n";
					str += Tower1.towerposiitons[coord].towerHead.getInfo("speed") + "\n";
					uIController.setTowerinfo(Tower1.towerposiitons[coord].name, str);
				}
	}
}

public class way
{
	List<Vector2> path;
	int x;
	int y;

	public way(int xx, int yy, List<Vector2> pat){
		path = new List<Vector2>(pat);
		x = xx;
		y= yy;
		path.Add(new Vector2(x, y));
		Vector2 coord = new Vector2(xx,yy);
		props.coordinaten.Add(coord);
		Logging.pathlog("NEW WAY: " + props.vListToString(path));
	}

	public bool checkNextStep(){
		if(props.goal.x == x && props.goal.y == y ){
			props.bestPath = path;

			Logging.pathlog("FINISH: Found best path, lenght: " + props.bestPath.Count.ToString());
			return true;
		}
		check(x,y+1);
		check(x,y-1);
		check(x+1,y);
		check(x-1,y);
		Logging.pathlog("REMOVED: " + props.vListToString(this.path));
		props.pathes.Remove(this);
		Logging.pathlog("CURRENT PATHES: "+props.pathes.Count);
		return false;
	}

	bool check(int x, int y){
		if(x<0 || y<0 || x>= props.mapscaleX || y>= props.mapscaleY){
			return false;
		}
		if(props.freemap[new Vector2(x,y)] == 1 || props.freemap[new Vector2(x,y)] == 4){
			if(!props.coordinaten.Contains(new Vector2(x,y))){
				way newway = new way(x,y,path);
				props.pathes.Add(newway);
			}
			
		}
		return false;
	}
}

static public class props
{
	static public List<Vector2> coordinaten = new List<Vector2>();
	public static List<way> pathes = new List<way>();

	public static Vector2 goal;

	public static Dictionary<Vector2, int> freemap;
	public static int mapscaleX, mapscaleY;
	public static List<Vector2> bestPath;
	public static List<GameObject> enemyList = new List<GameObject>();

	public static string vListToString(List<Vector2> list){
		string str = "";
		foreach(Vector2 vec in list){
			str = str +"["+ vec.x.ToString() +","+ vec.y.ToString() +"]";
		}
		return str;
	}
}

static public class Logging
{
	public static void pathlog(string msg){
		//Debug.Log("PATHFINDER:"+ msg);
	}
	public static void towerLog(string msg){
		Debug.Log("TOWER: "+ msg);
	}

	public static void debugLog(string msg){
		Debug.Log("DEBUGGING: "+ msg);
	}
}