using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public GameObject menu, ingame;
	    // ingame UI text elements
    public GameObject time, lives, gold, screen, income, wave;
        // ingame UI button elements
    public GameObject tower1btn, tower2btn, tower3btn, tower4btn, tower5btn;//, enemy1obj,enemy2obj,enemy3obj,enemy4obj,enemy5obj,enemy6obj;
    List<GameObject> towerbuttons = new List<GameObject>();
        // ingame UI tooltip & selection
	public GameObject tooltip1btnobj, tooltip2btnobj, tooltipbg, towerstats, towername, selectparticles;
        // Mainmenu UI elements
    public GameObject campainscreen, comingsoonscreen, menubtn1, menubtn2, menubtn3, menubtn4, menubtn5, level1, level2, level3;

    UnityEngine.UI.Text timetext, livestext, goldtext, towerinfotext, incometext, wavetext, btn1text,btn2text,btn3text,btn4text,btn5text;//enemy1text,enemy2text,enemy3text,enemy4text,enemy5text,enemy6text;
    TMPro.TextMeshProUGUI screentext;
    UnityEngine.UI.Button button1, button2, button3,button4,button5,tooltip1btn,tooltip2btn;//enemy1,enemy2,enemy3,enemy4,enemy5,enemy6,
    public GameObject gameControllerobj, gameMasterobj;
    GameMaster gameMaster;
    GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void init(){
        /*
        enemy1 = enemy1obj.GetComponent<UnityEngine.UI.Button>();
        enemy1.onClick.AddListener(clickEnemy1);
        enemy2 = enemy2obj.GetComponent<UnityEngine.UI.Button>();
        enemy2.onClick.AddListener(clickEnemy2);
        enemy3 = enemy3obj.GetComponent<UnityEngine.UI.Button>();
        enemy3.onClick.AddListener(clickEnemy3);
        enemy4 = enemy4obj.GetComponent<UnityEngine.UI.Button>();
        enemy4.onClick.AddListener(clickEnemy4);
        enemy5 = enemy5obj.GetComponent<UnityEngine.UI.Button>();
        enemy5.onClick.AddListener(clickEnemy5);
        enemy6 = enemy6obj.GetComponent<UnityEngine.UI.Button>();
        enemy6.onClick.AddListener(clickEnemy6);

        menu.SetActive(true);
        setTime(30);
        setLives(10);
        setIncome("0");
        setWave(1);
        */

        //init general stuff
        gameMaster = gameMasterobj.GetComponent<GameMaster>();
        gameController = gameControllerobj.GetComponent<GameController>();
        fixTooltip(null, false);
        InvokeRepeating("clearScreenText", 1, 1);

        //init ingame interface
        timetext = time.GetComponent<UnityEngine.UI.Text>();
        livestext = lives.GetComponent<UnityEngine.UI.Text>();
        goldtext = gold.GetComponent<UnityEngine.UI.Text>();
        screentext = screen.GetComponent<TMPro.TextMeshProUGUI>();
        incometext = income.GetComponent<UnityEngine.UI.Text>();
        wavetext = wave.GetComponent<UnityEngine.UI.Text>();

        //enemy1text = enemy1obj.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>();
        //enemy2text = enemy2obj.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>();
        //enemy3text = enemy3obj.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>();
        //enemy4text = enemy4obj.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>();
        //enemy5text = enemy5obj.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>();
        //enemy6text = enemy6obj.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>();

        tower1btn.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(clickButton1);
        tower2btn.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(clickButton2);
        tower3btn.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(clickButton3);
        tower4btn.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(clickButton4);
        tower5btn.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(clickButton5);

        towerbuttons.Add(tower1btn);
        towerbuttons.Add(tower2btn);
        towerbuttons.Add(tower3btn);
        towerbuttons.Add(tower4btn);
        towerbuttons.Add(tower5btn);

        tooltip1btn = tooltip1btnobj.GetComponent<UnityEngine.UI.Button>();
        tooltip1btn.onClick.AddListener(clickTooltip1btn);
        tooltip2btn = tooltip2btnobj.GetComponent<UnityEngine.UI.Button>();
        tooltip2btn.onClick.AddListener(clickTooltip2btn);

        //init mainmenu interface
        menuBtnObjList.Add(menubtn1);
        menuBtnObjList.Add(menubtn2);
        menuBtnObjList.Add(menubtn3);
        menuBtnObjList.Add(menubtn4);
        menuBtnObjList.Add(menubtn5);

        menuBtnList.Add(menubtn1.transform.GetChild(0).gameObject);
        menuBtnList.Add(menubtn2.transform.GetChild(0).gameObject);
        menuBtnList.Add(menubtn3.transform.GetChild(0).gameObject);
        menuBtnList.Add(menubtn4.transform.GetChild(0).gameObject);
        menuBtnList.Add(menubtn5.transform.GetChild(0).gameObject);

        menuBtnTextList.Add(menubtn1.transform.GetChild(0).gameObject.transform.GetChild(1).gameObject);
        menuBtnTextList.Add(menubtn2.transform.GetChild(0).gameObject.transform.GetChild(1).gameObject);
        menuBtnTextList.Add(menubtn3.transform.GetChild(0).gameObject.transform.GetChild(1).gameObject);
        menuBtnTextList.Add(menubtn4.transform.GetChild(0).gameObject.transform.GetChild(1).gameObject);
        menuBtnTextList.Add(menubtn5.transform.GetChild(0).gameObject.transform.GetChild(1).gameObject);

        menuScreenList.Add(campainscreen);
        menuScreenList.Add(comingsoonscreen);
        menuScreenList.Add(comingsoonscreen);
        menuScreenList.Add(comingsoonscreen);
        menuScreenList.Add(comingsoonscreen);

        level1.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(menuClickLevel1);
        level2.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(menuClickLevel2);
        level3.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(menuClickLevel3);
        menuBtnList.ToArray()[0].GetComponent<UnityEngine.UI.Button>().onClick.AddListener(menuClickCampain);
        menuBtnList.ToArray()[1].GetComponent<UnityEngine.UI.Button>().onClick.AddListener(menuClickSolowars);
        menuBtnList.ToArray()[2].GetComponent<UnityEngine.UI.Button>().onClick.AddListener(menuClickMaulwars);
        menuBtnList.ToArray()[3].GetComponent<UnityEngine.UI.Button>().onClick.AddListener(menuClickGuide);
        menuBtnList.ToArray()[4].GetComponent<UnityEngine.UI.Button>().onClick.AddListener(menuClickSettings);

        ingame.SetActive(false);
        menu.SetActive(false);
    }

    Color btnbg = new Color(0,0,0,0.7f);
    Color btnfg = new Color(1,1,1,1);
    string[] menuButtonNames = {"Campain","Solo Wars","Maul Wars","Guide","Settings"};
    List<GameObject> menuBtnObjList = new List<GameObject>(), menuBtnList = new List<GameObject>(), menuBtnTextList = new List<GameObject>(), menuScreenList = new List<GameObject>();
    public void buildMenu(int btnid){
        menu.SetActive(true);

        for(int ii = 0; ii < menuButtonNames.Length; ii++){
            menuBtnObjList.ToArray()[ii].GetComponent<UnityEngine.UI.Image>().color = Color.clear;
            menuBtnList.ToArray()[ii].GetComponent<UnityEngine.UI.Image>().color = btnfg;
            menuScreenList.ToArray()[ii].SetActive(false);
            setButtonTextActive(ii, false);
        }
        menuBtnObjList.ToArray()[btnid].GetComponent<UnityEngine.UI.Image>().color = btnbg;
        menuBtnList.ToArray()[btnid].GetComponent<UnityEngine.UI.Image>().color = Color.clear;
        menuScreenList.ToArray()[btnid].SetActive(true);
        //menuBtnList.ToArray()[btnid].transform.position = new Vector3(0,0,0);
        setButtonTextActive(btnid, true);

    }

    void setButtonTextActive(int menuBtnTextId, bool active){
        string msg;
        if(active){
            msg =  "<b><color=white>" + menuButtonNames[menuBtnTextId] + "</color></b>"; 
        } else {
            msg =  "<color=black>" + menuButtonNames[menuBtnTextId] + "</color>"; 
        }
        menuBtnTextList.ToArray()[menuBtnTextId].GetComponent<UnityEngine.UI.Text>().text = msg;
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    void menuClickLevel1(){
        menu.SetActive(false);
        ingame.SetActive(true);
        Database.setWorldTheme(0);
        gameController.startGame(1);
    }
    void menuClickLevel2(){
        menu.SetActive(false);
        ingame.SetActive(true);
        Database.setWorldTheme(1);
        gameController.startGame(1);
    }
    void menuClickLevel3(){
        menu.SetActive(false);
        ingame.SetActive(true);
        Database.setWorldTheme(2);
        gameController.startGame(1);
    }
    void menuClickCampain(){
        buildMenu(0);
    }
    void menuClickSolowars(){
        buildMenu(1);
    }
    void menuClickMaulwars(){
        buildMenu(2);
    }
    void menuClickGuide(){
        buildMenu(3);
    }
    void menuClickSettings(){
        buildMenu(4);
    }

int clearScreenIn = 0;
    void clearScreenText(){
        if(clearScreenIn <= 0){

        setScreenText("");
        }
        else clearScreenIn--;
    }
    public void changeTowerButtonText(int id, TowerClass tower, bool fullz){
        if(!fullz){
            towerbuttons.ToArray()[id-1].transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>().text = tower.name;
            towerbuttons.ToArray()[id-1].transform.GetChild(1).gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = tower.coast.ToString();
            towerbuttons.ToArray()[id-1].transform.GetChild(2).gameObject.GetComponent<UnityEngine.UI.Text>().text = "";
            towerbuttons.ToArray()[id-1].GetComponent<UnityEngine.UI.Image>().color = Database.elementColors[tower.element];  
        }else {
            towerbuttons.ToArray()[id-1].transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>().text = "";
            towerbuttons.ToArray()[id-1].transform.GetChild(1).gameObject.GetComponent<TMPro.TextMeshProUGUI>().text = tower.coast.ToString();
            string str = tower.towerHead.getInfo("effect") + "\n";
		    str += tower.towerHead.getInfo("range") + "\n";
		    str += tower.towerHead.getInfo("speed") + "\n";
            towerbuttons.ToArray()[id-1].transform.GetChild(2).gameObject.GetComponent<UnityEngine.UI.Text>().text = str;
            towerbuttons.ToArray()[id-1].GetComponent<UnityEngine.UI.Image>().color = Database.elementColors[tower.element]; 
        }     
    }
    /*
    public void changeEnemyButtonText(int id, string text, Color c){
        switch(id){
            case 1: 
                enemy1text.text = text;
                enemy1obj.GetComponent<UnityEngine.UI.Image>().color = c;
                break;
            case 2: 
                enemy2text.text = text;
                enemy2obj.GetComponent<UnityEngine.UI.Image>().color = c;
                break;
            case 3: 
                enemy3text.text = text;
                enemy3obj.GetComponent<UnityEngine.UI.Image>().color = c;
                break;
            case 4: 
                enemy4text.text = text;
                enemy4obj.GetComponent<UnityEngine.UI.Image>().color = c;
                break;
            case 5: 
                enemy5text.text = text;
                enemy5obj.GetComponent<UnityEngine.UI.Image>().color = c;
                break;
            case 6: 
                enemy6text.text = text;
                enemy6obj.GetComponent<UnityEngine.UI.Image>().color = c;
                break;
        }  
    }
    */

    void clickButton1(){
        Debug.Log("btn1 click.");
        gameMaster.createTower(1);
    }
    void clickButton2(){
        Debug.Log("btn2 click.");
        gameMaster.createTower(2);
    }
    void clickButton3(){
        Debug.Log("btn3 click.");
        gameMaster.createTower(3);
    }
    void clickButton4(){
        Debug.Log("btn4 click.");
        gameMaster.createTower(4);
    }
    void clickButton5(){
        Debug.Log("btn5 click.");
        gameMaster.createTower(5);
    }
    void clickEnemy1(){
        gameMaster.spawnEnemy(1, true);
    }
    void clickEnemy2(){
        
        gameMaster.spawnEnemy(2, true);
    }
    void clickEnemy3(){
        
        gameMaster.spawnEnemy(3, true);
    }
    void clickEnemy4(){
        
        gameMaster.spawnEnemy(4, true);
    }
    void clickEnemy5(){
        
        gameMaster.spawnEnemy(5, true);
    }
    void clickEnemy6(){
        
        gameMaster.spawnEnemy(6, true);
    }
    Tower1 tooltipTower;
    void clickTooltip1btn(){
        tooltipTower.upgradeTower();
    }
    void clickTooltip2btn(){
        tooltipTower.destroyTower(true);

    }
    

    public void setTime(int ms){
        string str;
        if(ms > 100){
            str = (ms / 10 +1).ToString();
        }
        else{
            str = (ms / 10).ToString() + "." + (ms % 10).ToString();
        }
        timetext.text = "Time: " + str;
    }

    public void setLives(int i){
        livestext.text = "Lives: " + i.ToString();
    }

    public void setGold(int i){
        goldtext.text = "Gold: " + i.ToString();
    }
    public void setTowerinfo(string name, string stats){
        towername.GetComponent<UnityEngine.UI.Text>().text = name + " Tower";
        towerstats.GetComponent<UnityEngine.UI.Text>().text = stats;
    }
    public void fixTooltip(Tower1 t, bool fix){
        if(fix){
            tooltipbg.GetComponent<UnityEngine.UI.Image>().color = new Color(.1f, .1f, .1f, .7f);
            
            if(t.upgradeToId != 0){
                int price = gameMaster.towercollection[t.upgradeToId].coast;
                tooltip1btnobj.SetActive(true);
                tooltip1btnobj.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>().text = "Upgrade\n("+price.ToString()+")";
                
            }else{
                tooltip1btnobj.SetActive(false);
            }
            tooltip2btnobj.SetActive(true);
            tooltip2btnobj.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Text>().text = "Sell\n("+(t.coast/2).ToString()+")";
            tooltipTower = t;
            selectparticles.transform.position = new Vector3(t.x*5, 2, t.y*5);
        }
        else {
            tooltipbg.GetComponent<UnityEngine.UI.Image>().color = new Color(.1f, .1f, .1f, .2f);
            tooltip1btnobj.SetActive(false);
            tooltip2btnobj.SetActive(false);
            selectparticles.transform.position = new Vector3(0, 100, 0);
            tooltipTower = null;
        }

    }
    public void setScreenText(string text){
        screentext.text = text;
        clearScreenIn = 2;
    }
    public void setIncome(string text){
        incometext.text = "Income: " + text;
    }
    public void setWave(int i){
        wavetext.text = "Wave: " + i.ToString();
    }
}
