using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tower1 : MonoBehaviour
{
    public string name;
    public int coast;
    public int upgradeToId;
    public TowerHead towerHead;

    GameObject target;
    public GameObject myGameObject;
    bool hasTarget = false;

    public GameObject projectile;

    public Database.Element element;

    public int level;

    public int x;
	public int y;

    GameMaster gameMaster;
    public static Dictionary<Vector2, Tower1> towerposiitons = new Dictionary<Vector2, Tower1>();
	
	public void init(Database.Element e, int l, string n, int co, int utid, TowerHead th)
	{
        name = n;
        coast = co;
        upgradeToId = utid;
        towerHead = new TowerHead(th);
        towerHead.setTower1(this);
        Logging.towerLog("init with "+towerHead.targetType+" towerHead.");
        level = l;
        element = e;
	}

    public void build(GameMaster gm, int xx, int yy){
        gameMaster = gm;
	    x = xx;
	    y = yy;
        towerposiitons.Add(new Vector2(x,y), this);
        Logging.towerLog("builded "+name+" at "+x.ToString() + ","+y.ToString());
        this.gameObject.GetComponent<MeshRenderer>().material.color = Database.elementColors[element];
        this.gameObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material.color = Database.elementColors[element];
        this.gameObject.transform.GetChild(1).gameObject.GetComponent<MeshRenderer>().material.color = Database.elementColors[element];

        Logging.towerLog("build towerHead: "+towerHead.targetType);
        towerHead.spawn(x,y);
        if(towerHead.speed != 0) {
            switch(towerHead.targetType){
            case "singleEnemy":
		        InvokeRepeating("singleEnemy", towerHead.speed, towerHead.speed);
                break;
            case "multiEnemy":
		        InvokeRepeating("multiEnemy", towerHead.speed, towerHead.speed);
                break;
            case "multiTower":
		        InvokeRepeating("multiTower", towerHead.speed, towerHead.speed);
                break;
            }
        
        }
        recalculateTowerbuffs();
    }
    void recalculateTowerbuffs(){
        foreach(Tower1 tower in towerposiitons.Values){
            tower.towerHead.resetBuffs();   
        }
        foreach(Tower1 tower in towerposiitons.Values){
            if(tower.towerHead.targetType == "multiTower"){
                tower.towerHead.multiTower();
            }
            
        }
    }

    void singleEnemy(){
        towerHead.singleEnemy();
    }
    void multiEnemy(){
        towerHead.multiEnemy();
    }
    void multiTower(){
        towerHead.multiTower();
    }
    

    public void upgradeTower(){
        gameMaster.destroyTower(new Vector2(x,y), 0);
        towerposiitons.Remove(new Vector2(x,y));
        gameMaster.spawnTower(new Vector2(x,y), upgradeToId);
        recalculateTowerbuffs();
        Destroy(myGameObject);
        //create betterone
    }

    public void destroyTower(bool sell){
        if(sell) 
            gameMaster.destroyTower(new Vector2(x,y), coast/2);
        else
            gameMaster.destroyTower(new Vector2(x,y), 0);

        towerposiitons.Remove(new Vector2(x,y));
        recalculateTowerbuffs();
        Destroy(myGameObject);
    }

	
	
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnMouseOver()
    {
        // Change the color of the GameObject to red when the mouse is over GameObject
       // m_Renderer.material.color = m_MouseOverColor;
       //Debug.Log("ground");
       gameMaster.mouseOverCoordinates(x,y);
    }
    public void shoot(GameObject target, float dmg, string dmgtype){
        GameObject clone = Instantiate(projectile, new Vector3(transform.position.x,4f,transform.position.z), transform.rotation);
        clone.GetComponent<Projectile>().init(target, dmg, dmgtype);
        clone.GetComponent<MeshRenderer>().material.color = Database.elementColors[element];
    }
	
	
}
public class TowerHead{

    public string targetType;
    //targetType: singleEnemy, multiEnemy, multiTower
    public string effect;
    //effect: hp, speed, dmg, range, value
    public float effectValue, speed;
    public int range;
    float dmgbuff = 0, rangebuff = 0, speedbuff = 0, valuebuff = 0;
    int x,y;
    Tower1 myTower;

    List<Vector2> positionsInRange = new List<Vector2>();

    public TowerHead(string t, string e, float ev, float s, int r){
        targetType = t;
        effect = e;
        effectValue = ev;
        speed = s;
        range = r;
    }
    public TowerHead(TowerHead th){
        targetType = th.targetType;
        effect = th.effect;
        effectValue = th.effectValue;
        speed = th.speed;
        range = th.range;
    }
    public void setTower1(Tower1 t){
        myTower = t;
    }

    public void spawn(int xx, int yy){
        x = xx;
        y = yy;
        if(speed == 0){
            switch(targetType){
            case "singleEnemy":
		        singleEnemy();
                break;
            case "multiEnemy":
		        multiEnemy();
                break;
            case "multiTower":
		        multiTower();
                break;
            }
        }
        if(range > 0){
                addCoordToPositionsInRange();
            }
        
    }

    void addCoordToPositionsInRange(){
        for(int ii=0;ii<range+1;ii++){
            for(int jj=0;jj<range+1;jj++){
                if(Mathf.Sqrt(ii*ii+jj*jj)<=range){
                    if(ii+jj > 0){
                        positionsInRange.Add(new Vector2(ii+x,jj+y));
                        }
                    if(jj > 0){
                        positionsInRange.Add(new Vector2(ii+x,-jj+y));
                        if(ii > 0){
                            positionsInRange.Add(new Vector2(-ii+x,jj+y));
                            positionsInRange.Add(new Vector2(-ii+x,-jj+y));
                        }
                    }
                    else if(ii > 0){
                        positionsInRange.Add(new Vector2(-ii+x,jj+y));
                    }
                }
            }
        }

        

    }    

     bool hasTarget = false;
     Enemy1 target = new Enemy1();
    public void singleEnemy(){
        if(hasTarget){
            if(!Enemy1.enemyposiitons.ContainsKey(target) || !positionsInRange.Contains(Enemy1.enemyposiitons[target])){
                hasTarget = false;
                target = null;
            }
            
        }
        if(!hasTarget){

            lookForTarget();
        }
        if(hasTarget){

            //Debug.Log("SHOOT!!!");
            if(effect == "hp"){
                    myTower.shoot(target.myGameObject, effectValue+effectValue*dmgbuff, effect);
            }
            else if(effect == "speed"){
                myTower.shoot(target.myGameObject, effectValue+effectValue*dmgbuff, effect);
            }
        }


        

    }
    void lookForTarget(){
            foreach(KeyValuePair<Enemy1,Vector2> enemy in Enemy1.enemyposiitons){
                if(range == 0 || positionsInRange.Contains(enemy.Value)){
                    target = enemy.Key;
                    hasTarget = true;
                }
            }
    }
    public void multiEnemy(){
        Dictionary<Enemy1, Vector2> targets = new Dictionary<Enemy1, Vector2>(Enemy1.enemyposiitons);
        
            foreach(KeyValuePair<Enemy1,Vector2> enemy in targets){
                if(range == 0 || positionsInRange.Contains(enemy.Value)){
                    if(effect == "hp"){
                        if(Enemy1.enemyposiitons.ContainsKey(enemy.Key)){
                            enemy.Key.hit((int)(effectValue+effectValue*dmgbuff));
                        }
                    }
                    else if(effect == "speed"){
                        enemy.Key.slow(effectValue+effectValue*dmgbuff, 2);
                    }
                }
            }
    }
    public void multiTower(){
        foreach(KeyValuePair<Vector2,Tower1> tower in Tower1.towerposiitons){
                if(range == 0 || positionsInRange.Contains(tower.Key)){
                    if(effect == "dmg"){
                        tower.Value.towerHead.buffdmg(effectValue);
                    }
                    else if(effect == "range"){

                    }
                }
            }
    }

    List<Vector2> tmppositionsInRange = new List<Vector2>();
    float tmprange=0;
    void increaseRange(){

    }
    void doActionSpeed(){
        
    }
    void doActionRange(){
        
    }

    public string getInfo(string info){
        string str = "";
        switch(info){
            case "effect":
                str += buildEffectString();
                break;
            case "range":
                str += range.ToString() + "m ("+targetType + ")";
                break;
            case "speed":
                str += (1/speed).ToString() + "x/s";
                break;
        }
        return str;
    }
    string buildEffectString(){
        string str = "";
        if(effect == "hp"){
            str += "DMG: " + effectValue.ToString();
            if(dmgbuff > 0){
                str += " ( +"+(dmgbuff*effectValue).ToString()+" )";
            }
        }
        else if(effect == "speed"){
            if(targetType == "multiTower"){
                str += "Incr. Speed: " + (effectValue*100).ToString()+"%";
            }
            else {
                str += "Decr. Speed: -" + (effectValue*100).ToString()+"%";
            }
        }
        else if(effect == "dmg"){
            str += "Incr. DMG: " + (effectValue*100).ToString()+"%";
        }
        else if(effect == "range"){
            str += "Incr. Range: " + (effectValue*100).ToString()+"%";
        }
        else if(effect == "value"){
            str += "Incr. Value: " + (effectValue*100).ToString()+"%";
        }
        return str;
    }
    public void buffdmg(float buff){
        dmgbuff += buff;
    }
    public void buffrange(float buff){
        rangebuff += buff;
    }
    public void buffspeed(float buff){
        speedbuff += buff;
    }
    public void buffvalue(float buff){
        valuebuff += buff;
    }
    public void resetBuffs(){
        dmgbuff = 0;
        rangebuff = 0;
        speedbuff = 0;
        valuebuff = 0;
    }

}

