using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Database : MonoBehaviour
{  
    static int[,] world = {{2,2,2,2,2,2,2,2,2},{2,1,1,3,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,1,1,1,2},{2,1,1,1,1,4,1,1,2},{2,2,2,2,2,2,2,2,2}};
	static int mapsizeX = 9, mapsizeY;
    public static int worldSizeFactor = 5;

    static int wavecounter = 0;
    static List<Vector2> wave = new List<Vector2>();

    static Dictionary<int, TowerClass> towercollection = new Dictionary<int, TowerClass>();
	static Dictionary<int, EnemyClass> enemycollection = new Dictionary<int, EnemyClass>();
    static GameObject tower1, tower2, tower3, enemy1, enemy2, enemy3, enemy4;
    public GameObject t1,t2,t3,e1,e2,e3,e4;
    public Material sg,sw,sl,gg,gw,gl,lg,lw,ll;
    public GameObject ground, wall, start, end, land, startportal, endportal, upgrade;
    static List<Material> themeMaterials = new List<Material>();
    public static Dictionary<Element, Color> elementColors = new Dictionary<Element, Color>();
    public static Dictionary<WorldObjectType,GameObject> worldObjects = new Dictionary<WorldObjectType, GameObject>();

    public enum WorldObjectType {Ground, Wall, Land, Start, End, Startportal, Endportal, Upgrade};
    public enum Element {Hass, Freiheit, Liebe, Mangel};

    public void initDatabase(){
        mapsizeY = world.Length / mapsizeX;
        tower1 = t1;
        tower2 = t2;
        tower3 = t3;
        enemy1 = e1;
        enemy2 = e2;
        enemy3 = e3;
        enemy4 = e4;
        themeMaterials.Add(sg);
        themeMaterials.Add(sw);
        themeMaterials.Add(sl);
        themeMaterials.Add(gg);
        themeMaterials.Add(gw);
        themeMaterials.Add(gl);
        themeMaterials.Add(lg);
        themeMaterials.Add(lw);
        themeMaterials.Add(ll);
        worldObjects.Add(WorldObjectType.Ground,ground);
        worldObjects.Add(WorldObjectType.Wall,wall);
        worldObjects.Add(WorldObjectType.Land,land);
        worldObjects.Add(WorldObjectType.Start,start);
        worldObjects.Add(WorldObjectType.End,end);
        worldObjects.Add(WorldObjectType.Startportal,startportal);
        worldObjects.Add(WorldObjectType.Endportal,endportal);
        worldObjects.Add(WorldObjectType.Upgrade,upgrade);
        elementColors.Add(Element.Hass, new Color(1, .5f, .5f, 1));
        elementColors.Add(Element.Freiheit, new Color(.5f, 1, .5f, 1));
        elementColors.Add(Element.Liebe, new Color(1, 1, .5f, 1));
        elementColors.Add(Element.Mangel, new Color(.5f, .5f, 1, 1));

        //worldObjects[WorldObjectType.Startportal].transform.GetChild(0).gameObject.GetComponent<ParticleSystem>().playbackSpeed = .1f;

        setWorldTheme(1);
        player1towers();
        player1enemies();
    }
    static void player1towers(){
        towercollection.Add(1, new TowerClass(Element.Mangel, 1, "Buffer", 20, tower2, 6, new TowerHead("multiTower", "dmg", .1f, 0, 2)));
        towercollection.Add(2, new TowerClass(Element.Liebe, 1, "Shooter", 80, tower1, 7, new TowerHead("singleEnemy", "hp", 1, 1, 3)));
        towercollection.Add(3, new TowerClass(Element.Freiheit, 1, "Slooower", 200, tower3, 0, new TowerHead("multiEnemy", "speed", .4f, 1, 3)));
        towercollection.Add(4, new TowerClass(Element.Hass, 1, "Fasti", 300, tower1, 0, new TowerHead("singleEnemy", "hp", 3, .5f, 1)));
        towercollection.Add(5, new TowerClass(Element.Hass, 1, "Smasher", 500, tower2, 0, new TowerHead("multiEnemy", "hp", 1, 1, 3)));

        towercollection.Add(6, new TowerClass(Element.Mangel, 2, "<color=green>Buffer 2</color>", 100, tower3, 0, new TowerHead("multiTower", "dmg", .5f, 0, 2)));
        towercollection.Add(7, new TowerClass(Element.Liebe, 2, "<color=green>Shooter 2</color>", 300, tower3, 0, new TowerHead("singleEnemy", "hp", 5, 1, 3)));
    }
    static void testtowers(){
        /*
        towercollection.Add(1, new TowerClass(Color.green, "S-Tower", 10, tower1, 2, new TowerHead("singleEnemy", "hp", 1, 1, 5)));
        towercollection.Add(2, new TowerClass(Color.black, "AOE-Tower", 100, tower3, 0, new TowerHead("multiEnemy", "hp", 5, .5f, 3)));
        towercollection.Add(3, new TowerClass(Color.white, "DEATH-Tower", 100, tower2, 0, new TowerHead("multiEnemy", "hp", 500, 0, 30)));
        towercollection.Add(4, new TowerClass(Color.yellow, "BUFF-Tower", 100, tower1, 0, new TowerHead("multiTower", "dmg", .5f, 0, 3)));
        towercollection.Add(5, new TowerClass(Color.blue, "Slow-Tower", 100, tower2, 0, new TowerHead("multiEnemy", "speed", .4f, 1, 3)));
        */
    }

    static void testenemies(){
        enemycollection.Add(1, new EnemyClass("Enorm",Color.green, 50, 2, enemy3, 8, 2, Enemy1.enemytype.FLYING));
        enemycollection.Add(2, new EnemyClass("Esmal",Color.yellow, 10, 4, enemy3, 8, 1.3f, Enemy1.enemytype.FLYING));
        enemycollection.Add(3, new EnemyClass("Ebig",Color.blue, 100, 1, enemy3, 8, 3, Enemy1.enemytype.FLYING));
    }
    static void player1enemies(){
        enemycollection.Add(1, new EnemyClass("Enorm",Color.yellow, 10, 2, enemy4, 20, 2, Enemy1.enemytype.NORMAL));
        enemycollection.Add(2, new EnemyClass("Efast",Color.red, 5, 5, enemy4, 25, 1.2f, Enemy1.enemytype.NORMAL));
        enemycollection.Add(3, new EnemyClass("Ebig",Color.green, 30, 1, enemy4, 30, 2.5f, Enemy1.enemytype.NORMAL));
    }
    public static List<Vector2> getNextWave(){
        wavecounter++;
        wave = new List<Vector2>();
        if(wavecounter == 1){
            wave.Add(new Vector2(1,30));
            wave.Add(new Vector2(1,30));
            wave.Add(new Vector2(1,0));
        }
        else if(wavecounter == 2){
            wave.Add(new Vector2(1,30));
            wave.Add(new Vector2(1,30));
            wave.Add(new Vector2(1,30));
            wave.Add(new Vector2(2,10));
            wave.Add(new Vector2(2,10));
            wave.Add(new Vector2(2,10));
            wave.Add(new Vector2(2,0));
        }
        else if(wavecounter == 3){

            wave.Add(new Vector2(1,20));
            wave.Add(new Vector2(1,20));
            wave.Add(new Vector2(1,20));
            wave.Add(new Vector2(1,50));
            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,0));
        }
        else if(wavecounter >= 4){

            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,20));
            wave.Add(new Vector2(1,10));
            wave.Add(new Vector2(1,10));
            wave.Add(new Vector2(1,10));
            wave.Add(new Vector2(1,50));
            wave.Add(new Vector2(3,50));
            wave.Add(new Vector2(3,50));
            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,5));
            wave.Add(new Vector2(2,0));
        }
        else if(wavecounter == 5){

        }
        else if(wavecounter == 6){

        }
        else if(wavecounter == 7){

        }
        else if(wavecounter == 8){

        }
        else if(wavecounter == 9){

        }
        return wave;
    }

    public static Dictionary<int, TowerClass> getTowerCollection(){
        return towercollection;
    }
    public static Dictionary<int, EnemyClass> getEnemyCollection(){
        return enemycollection;
    }

    public static int[,] getWorld(){
        return world;
    }
    public static Vector2 getWorldSize(){
        return new Vector2(mapsizeX, mapsizeY);
    }
    public static void setWorldTheme(int id){
        worldObjects[WorldObjectType.Ground].GetComponent<Renderer>().material = themeMaterials.ToArray()[id*3];
        worldObjects[WorldObjectType.Wall].GetComponent<Renderer>().material = themeMaterials.ToArray()[id*3+1];
        worldObjects[WorldObjectType.Land].GetComponent<Renderer>().material = themeMaterials.ToArray()[id*3+2];
    }
}
