using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    // Start is called before the first frame update

    GameObject enemy;
    bool hasEnemy = false;
    float speed = .8f;
    float dmg = 0;
    public GameObject myGameObject;
    string dmgtype;
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(hasEnemy && enemy == null){
            die();
        }
        else if(hasEnemy){
            Vector3 direction = enemy.transform.position - myGameObject.transform.position;
            float shootsteps = direction.magnitude / speed;
            if(shootsteps <= 1){
                if(Enemy1.enemyposiitons.ContainsKey(enemy.GetComponent<Enemy1>())){
                    collision();
                }
                die();
            }
            myGameObject.transform.position = myGameObject.transform.position + (direction / shootsteps);
        }
    }

    void collision(){
        if(dmgtype == "hp"){
            enemy.GetComponent<Enemy1>().hit(dmg);
        }
        else if(dmgtype == "speed"){
            enemy.GetComponent<Enemy1>().slow(dmg, 2);
        }
    }

    void die(){
        Destroy(myGameObject);
    }

    public void init(GameObject enem, float d, string dt){
        enemy = enem;
        hasEnemy = true;
        dmg = d;
        dmgtype = dt;
    }
}
