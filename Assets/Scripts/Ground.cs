using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
	public int x;
	public int y;

    GameMaster gameMaster;
	
	public void init(Vector2 coord, GameMaster gm)
	{
	x = (int)coord.x;
	y = (int)coord.y;
    gameMaster = gm;
	}
	
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseOver()
    {
        // Change the color of the GameObject to red when the mouse is over GameObject
       // m_Renderer.material.color = m_MouseOverColor;
       //Debug.Log("ground");
       gameMaster.mouseOverCoordinates(x,y);
    }
	
}
