using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : MonoBehaviour
{
public float movespeed, normalmovespeed;
List<Vector2> path;
Vector2[] patharray;
int step = 0;

bool moving = false;

float moveprogress = 0f;

float generalspeed = 0.01f;
public int goldForKill;
public float size;

public int hpmax;
public float hp;
public GameObject hpbar;
public Color color;
public static Dictionary<Enemy1, Vector2> enemyposiitons = new Dictionary<Enemy1, Vector2>();
float initHPBarScale;
GameMaster gameMaster;
public  GameObject myGameObject;

public enum enemytype {NORMAL, FLYING, GHOST, BOSS};

public enemytype type;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void init(int h, int s, Color c, int gfk, float si, enemytype et){
        hpmax = h;
        hp = hpmax;
        movespeed = s;
        normalmovespeed = movespeed;
        color = c;
        goldForKill = gfk;
        size = si;
        type = et;
    }

    // Update is called once per frame
    Vector3 direction = new Vector3();
    void FixedUpdate()
    {
        if (moving){
            //path[5] = null;
            //Vector3 direction = (new Vector3(path[step+1,0],0,path[step+1,1])) - (new Vector3(path[step,0],0,path[step,1]));
            //direction = new Vector3(patharray[step+1].x-patharray[step].x, 0 , patharray[step+1].y-patharray[step].y);
            
            float stepsize = generalspeed * movespeed; 
            transform.position = transform.position + direction * stepsize * 5;
            moveprogress += stepsize;
            if(moveprogress >= 1){  
                moveprogress = 0;
                step++;
                if(step == patharray.Length-1){
                    moving = false;
                    gameMaster.looseLive();
                    die();
                    return;
                }
                direction = new Vector3(patharray[step+1].x-patharray[step].x, 0 , patharray[step+1].y-patharray[step].y);
                setRotation();
            }
            //if(transform.position)
        }
    }

    void setRotation(){
        if(patharray[step+1].x-patharray[step].x == 1){

                myGameObject.transform.eulerAngles = new Vector3(0.0f, 90.0f, 0.0f);
        }
        if(patharray[step+1].x-patharray[step].x == -1){
            
                myGameObject.transform.eulerAngles = new Vector3(0.0f, 270.0f, 0.0f);
        }
        if(patharray[step+1].y-patharray[step].y == 1){
            
                myGameObject.transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
        }
        if(patharray[step+1].y-patharray[step].y == -1){
            
                myGameObject.transform.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
        }
        if(CameraController.orientationMode == 1){
                myGameObject.transform.GetChild(2).gameObject.transform.eulerAngles = new Vector3(0.0f, 90.0f, 0.0f);
                //myGameObject.transform.GetChild(3).gameObject.transform.eulerAngles = new Vector3(0.0f, 90.0f, 0.0f);
        }
        if(CameraController.orientationMode == 2){
                myGameObject.transform.GetChild(2).gameObject.transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
                //myGameObject.transform.GetChild(3).gameObject.transform.eulerAngles = new Vector3(0.0f, 0.0f, 0.0f);
        }
    }
    void checkPosition(){
        int xx = (int)(myGameObject.transform.position.x+2.5f)/5;
        int yy = (int)(myGameObject.transform.position.z+2.5f)/5;
        enemyposiitons[this] = new Vector2(xx,yy);
        //Debug.Log(enemyposiitons.Count.ToString());
    }

    void die(){
        props.enemyList.Remove(myGameObject);
        gameMaster.killEnemy(goldForKill);
        enemyposiitons.Remove(this);
        Destroy(myGameObject);
    }

    public void spawn(List<Vector2> optpath, GameMaster gm){
        gameMaster = gm;
        path = new List<Vector2>(optpath);
        patharray = path.ToArray();
        moving = true;
        direction = new Vector3(patharray[step+1].x-patharray[step].x, 0 , patharray[step+1].y-patharray[step].y);
        //this.gameObject.GetComponent<MeshRenderer>().material.color = color;
        //this.gameObject.transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().material.color = color;
        this.gameObject.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.SetColor("_Color", color);
        enemyposiitons.Add(this, new Vector2(-10, -10));
        initHPBarScale = hpbar.transform.localScale.x;
        //Debug.Log("Movespeed = "+movespeed.ToString());
        InvokeRepeating("checkPosition", 1f/movespeed, 1f/movespeed);
        myGameObject.GetComponent<Animator>().SetFloat("speed", .3f*movespeed/size);
        setRotation();
        if(type.Equals(enemytype.FLYING)){
            transform.position = transform.position + new Vector3(0,3,0);
        }
    }

    public void hit(float dmg){
        hp = hp - (int)dmg;
        if(hp <= 0){
            die();
        }
        hpbar.transform.localScale = new Vector3(initHPBarScale * hp/hpmax, hpbar.transform.localScale.y, hpbar.transform.localScale.z);
        hpbar.transform.localPosition = new Vector3((initHPBarScale/2-(hp/hpmax*initHPBarScale/2)), hpbar.transform.localPosition.y, hpbar.transform.localPosition.z);

    }

    public void slow(float intensity, int time){
        
        if(movespeed == normalmovespeed){
            movespeed -= movespeed*intensity;
            hpbar.GetComponent<MeshRenderer>().material.color = new Color(.6f,.6f,1);
            StartCoroutine(waitingForUnlock(5));
        }
        else if(intensity > (normalmovespeed-movespeed)/normalmovespeed){
            movespeed = normalmovespeed*intensity;
        }
        }
    IEnumerator waitingForUnlock(float s){
		yield return new WaitForSeconds(s);
        movespeed = normalmovespeed;
        hpbar.GetComponent<MeshRenderer>().material.color = Color.green;
	}
	

}
