using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    // Start is called before the first frame update
    /* gamemode = 1 : marathon
    * gamemode = 2 : maul self wars
    * gamemode = 3 : maul wars
    */
    int gamemode = 1;
    
    public GameObject gameMasterobj, canvas, camera;
    UIController uIController;
    GameMaster gameMaster;
	//Dictionary<int, TowerClass> towercollection = new Dictionary<int, TowerClass>();
	//Dictionary<int, EnemyClass> enemycollection = new Dictionary<int, EnemyClass>();
    //public GameObject tower1, tower2, tower3, enemy1, enemy2, enemy3, enemy4;
	
	
    void Start()
    {
        gameObject.GetComponent<Database>().initDatabase();
        //Database.initDatabase();
        gameMaster = gameMasterobj.GetComponent<GameMaster>();
        uIController = canvas.GetComponent<UIController>();
        
        uIController.init();
        uIController.buildMenu(0);
    }

    public void startGame(int mode){
        if(mode == 1){
            gamemode1();
        }
        if(mode == 2){
            gamemode2();
        }
    }
    void gamemode1(){
        gameMaster.createWorld(Database.getWorld(), (int)Database.getWorldSize().x);
        gameMaster.setTowers(Database.getTowerCollection());
        gameMaster.setEnemies(Database.getEnemyCollection());
        camera.GetComponent<CameraController>().initCamera();

        gameMaster.setWave(Database.getNextWave(), 60);
        InvokeRepeating("gameMode1Controller", .1f, .1f);
    }
    
    bool waveset = false;
    int gameControllerMode = 1;
    void gameMode1Controller(){
        gameMaster.gameModeFlowController(); 
    }
    void gamemode2(){
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public class TowerClass{
    public string name;
    public int coast;
    public GameObject towerGameObject;
    public int upgradeToId;
    public TowerHead towerHead;
    public Database.Element element;

    public int level;

	
	public TowerClass(Database.Element e, int l, string n, int co, GameObject go, int utid, TowerHead th)
	{
        name = n;
        coast = co;
        towerGameObject = go;
        upgradeToId = utid;
        towerHead = th;
        level = l;
        element = e;
	}

    public void instantiateNew(Tower1 to){
        to.init(element, level, name, coast, upgradeToId, towerHead);
        //return gameObject;
    }
}

public class EnemyClass{
	public int speed;
    public string name;
    public int hp;
    public int goldForKill;
    public Color color;
    GameObject gameObject;
    public float size;
    public Enemy1.enemytype type;
	
	public EnemyClass(string n, Color c, int h, int s, GameObject go, int gfk, float si, Enemy1.enemytype et)
	{
        name = n;
        hp = h;
        speed = s;
        color = c;
        gameObject = go;
        goldForKill = gfk;
        size = si;
        type = et;
	}

    public GameObject instantiateNew(){
        gameObject.GetComponent<Enemy1>().init(hp, speed, color, goldForKill, size, type);
        gameObject.transform.localScale = new Vector3(size, size, size);
        return gameObject;
    }
}

