using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Vector3 baseposition1, baseposition2, rotationoffset, movementoffset;
    public static int orientationMode = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void initCamera(){
        float x = Database.getWorldSize().x * Database.worldSizeFactor;
        float y = (Database.getWorldSize().x + 2 ) * Database.worldSizeFactor;
        if(orientationMode == 1){
            transform.position = new Vector3(x*1.5f,x*2,y/2);
            transform.eulerAngles = new Vector3(65, 270, 0);
        }
        if(orientationMode == 2){
            transform.position = new Vector3(x/2,y*2,y*1.5f);
            transform.eulerAngles = new Vector3(65, 180, 0);
        }
    }
}
